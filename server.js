// IMPORTACIONES
const express = require('express');
const morgan = require('morgan');
const hbs = require('hbs');
const path = require('path');

// INICIALIZACIONES
const server = express();

// SETTINGS
server.set('port', 5001);
server.set('view engine', 'hbs');
hbs.registerPartials(path.join(__dirname, '/views/partials'));
// server.set('view engine', 'ejs');

//console.log(path.join(__dirname, '/views'));

// MIDDLEWARES
server.use(express.json());
server.use(express.urlencoded({extended: true}));
server.use(morgan('dev'));
server.use(express.static('public'));

server.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});

// RUTAS

// rutas para la aplicación
server.get('/', (req, res) => {
    res.render('home');
});
server.get('/videos', (req, res) => {
    res.render('videos');
});

server.use('/personas', require('./routes/personas'));

// rutas de API
server.use('/api/videojuegos', require('./routes/API/videojuegos'));



server.listen(server.get('port'), () => {
    console.log(`Servidor está corriendo en el puerto ${server.get('port')}`);
});