const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    res.send('ESTÁS EN LA PÁGINA DE PERSONAS')
});

router.post('/', (req, res) => {
    // console.log(req.body);
    const datosPersonas = req.body;
    const mensaje2 = req.body.otroMensaje;
    /* res.send('Datos de personas'); */
    res.status(201).json({
        ok: true,
        mensaje: 'Información del cliente llegó correctamente',
        datosPersonas,
        mensajeAlterno: mensaje2
    });
});



router.get('/juegos', (req, res) => {
    // Obteniendo datos de la base
    const data = [
        { juego: 'Spiderman PS5' },
        { juego: 'The Legend of Zelda' },
        { juego: 'League of Legends' }
    ];
    res.render('juegos', { videojuegos: data });
});

router.get('/integrantes', (req, res) => {
    res.send('ESTÁS EN LA PÁGINA DE INTEGRANTES')
});

module.exports = router;