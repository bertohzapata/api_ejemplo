const express = require('express');
const router = express.Router();
const pool = require('../../database/database');

router.get('/', (req, res)=> {
    pool.getConnection((err, connection)=> {
         if (err) throw err;

         connection.query('SELECT * FROM videojuegos', (err, videojuegos, fields) => {
             if (err) throw err;
             res.status(200).json({
                 ok: true,
                 videojuegos
             });
         });
    });
});

router.post('/', (req, res)=> {
    const juegos = req.body;

    pool.getConnection((err, connection)=> {
         if (err) throw err;

         connection.query('INSERT INTO videojuegos SET ?', juegos, (err, resultadoInsercion, fields) => {
             if (err) throw err;

             res.status(200).json({
                 ok: true,
                 resultadoInsercion
             });
         });
    });
});

module.exports = router;