const mysql = require('mysql');

const pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'root',
    password: '12345',
    database: 'sistemadeprueba'
});


pool.getConnection((err, connection) => {
    if (err) {
        throw err;
        console.log(err)
        
    }
    console.log('Base de datos conectada');
    connection.release();
});


module.exports = pool;